// TASK 1

const task1_num1 = parseFloat(prompt("Enter first number:"));
const task1_num2 = parseFloat(prompt("Enter second number:"));

function divideNumbers(task1_num1, task1_num2) {
    if (task1_num2 === 0) {
        return "Can not be divided by 0";
    } else {
        // return task1_num1 / task1_num2; НЕ ПОНИМАЮ ЗАДАНИЕ, САМИ ВЫБЕРИТЕ ПОДХОДЯЩИЙ ВАРИАНТ
        return task1_num1 % task1_num2;
    }
}

let task1_result = divideNumbers(task1_num1, task1_num2);

console.log(task1_result);
alert(task1_result);

// TASK 2

const num1 = validator("Enter the first number:");
const num2 = validator("Enter the second number:");

function validator(promptMessage) {
    let userInput;
    do {
        userInput = prompt(promptMessage);
        if (userInput === null) {
            alert("You canceled the input!");
            throw new Error("User canceled the input");
        }
        userInput = parseFloat(userInput);
    } while (isNaN(userInput));
    return userInput;
}

let operation;
do {
    operation = prompt("Enter a mathematical operation (+, -, *, /):");
    if (operation === null) {
        alert("You canceled the operation.");
        throw new Error("User canceled the operation");
    }
} while (!['+', '-', '*', '/'].includes(operation));

function calculator(a, b, op) {
    switch (op) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return b !== 0 ? a / b : alert("Can not divide by 0!");
        default:
            return "Invalid operation";
    }
}

const result = calculator(num1, num2, operation);
console.log(`Result of ${num1} ${operation} ${num2}:`, result);
alert(result);





